***Project Title:*** **Urban Transport Mapping for Resilience in Kigali**


***Data collected by:*** GoMetro


***Funding:*** GFDRR (the Global Facility for Disaster Reduction and Recovery) and The World Bank


***Project Aim:*** The project aims to collect data during “dry” and “flooded” conditions to develop an understanding of the accessibility to jobs and other opportunities in Kigali during the wet and dry seasons.

***Data Collection Period:***

   ***- Dry Condition*** - The data collection exercise for the dry condition took place over 3 weeks from the ***26th of February to the 4th of May 2019.***

   ***- Wet Condition*** - The data collection exercise for the wet condition took place over 3 weeks from the ***2nd of December to the 10th of December 2020*** 
